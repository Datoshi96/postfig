import { Post } from "@/models";
import { createSlice } from "@reduxjs/toolkit";

export const PageEmptyState: Post =
  {
    id: 0,
    title: '',
    body: '',
  };

export const pageSlice = createSlice({
  name: "page",
  initialState: PageEmptyState,
  reducers: {
    createPage: (state, action) => action.payload,
    modifyPage: (state, action) => ({ ...state, ...action.payload }),
    resetPage: () => PageEmptyState,
  },
});

export const {
  createPage,
  modifyPage,
  resetPage,
} = pageSlice.actions;

export default pageSlice.reducer;