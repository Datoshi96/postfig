import { Post } from '@/models';
import axios from 'axios';

export const serviceGetPhoto = (id:number) => {
   return axios.get(`https://jsonplaceholder.typicode.com/photos/${id}`);
}

export const serviceDeletePost = (id:number) => {
   return axios.delete(`https://jsonplaceholder.typicode.com/posts/${id}`,{
      method: 'DELETE',
   });
}

export const serviceUpdatePost = (id:number,body:Post) => {
   return axios.put(`https://jsonplaceholder.typicode.com/posts/${id}`,{
      body: JSON.stringify({
         id: body.id,
         title: body.title,
         body: body.body,
      })
   },{
      method: 'PUT',
   });
}
