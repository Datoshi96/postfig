import { Post } from "@/models";
import { createSlice } from "@reduxjs/toolkit";

export const PostEmptyState: Post[] = [
  {
    id: 0,
    title: '',
    body: '',
  },
];

export const postSlice = createSlice({
  name: "post",
  initialState: PostEmptyState,
  reducers: {
    createPost: (state, action) => action.payload,
    modifyPost: (state, action) => ({ ...state, ...action.payload }),
    addPost(state, action) {
      state.push(action.payload);
    },
    addNewPost(state, action) {
      state.unshift(action.payload);
    },
    resetPost: () => PostEmptyState,
  },
});

export const {
  createPost,
  modifyPost,
  resetPost,
  addPost,
  addNewPost,
} = postSlice.actions;

export default postSlice.reducer;