"use client";
import {
  AppBar,
  Avatar,
  Box,
  Button,
  Container,
  Icon,
  IconButton,
  Menu,
  MenuItem,
  Toolbar,
  Tooltip,
  Typography,
} from "@mui/material";
import Person2Icon from '@mui/icons-material/Person2';
import React from "react";
import MenuIcon from "@mui/icons-material/Menu";
import logo from "@/public/icono.png";
import Image from "next/image";
import { useRouter } from "next/navigation";

export type NavBarProps = {
  // types...
};

const pages = ["Inicio","Listado"];
const settings = ["Profile", "Logout"];

const NavBar: React.FC<NavBarProps> = () => {
  const router = useRouter();
  const [anchorElNav, setAnchorElNav] = React.useState<null | HTMLElement>(
    null
  );
  const [anchorElUser, setAnchorElUser] = React.useState<null | HTMLElement>(
    null
  );

  const handleOpenNavMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleRouter = (route:string) =>{
    if(route === 'Inicio'){
      router.push('/');
    }else if(route === 'Listado'){
      router.push('/listado');
    }
    setAnchorElUser(null);
  };
  const handleCloseNavMenu = () => {
    router.push('');
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };
  return (
    <AppBar
      position="fixed"
      style={{ background: "#D99C11", fontFamily: "Edo" }}
    >
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <Icon
            sx={{
              width: 104,
              height: 65,
              display: { xs: "none", md: "flex" },
              mr: 1,
            }}
          >
            <Image src={logo} alt="" width={104} height={65}></Image>
          </Icon>
          <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleOpenNavMenu}
              sx={{ color: "#000000" }}
            >
              <MenuIcon sx={{ color: "#000000" }} />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "left",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "left",
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: "block", md: "none" },
              }}
            >
              {pages.map((page) => (
                <MenuItem
                  sx={{
                    background: "#D99C11",
                    paddingTop: 0,
                    paddingBottom: 0,
                    fontWeight:600,
                  }}
                  key={page}
                  onClick={()=>handleRouter(page)}
                >
                  <Typography
                    sx={{ color: "#000000",fontWeight:'600' }}
                    textAlign="center"
                  >
                    {page}
                  </Typography>
                </MenuItem>
              ))}
            </Menu>
          </Box>
          <Icon
            sx={{
              mr: 2,
              display: { xs: "flex", md: "none" },
              flexGrow: 1,
              width: 104,
              height: 65,
            }}
          >
            <Image src={logo} alt="" width={104} height={65}></Image>
          </Icon>
          <Box
            style={{ background: "#D99C11", fontFamily: "Edo" }}
            sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}
          >
            {pages.map((page) => (
              <Button
                key={page}
                onClick={()=>handleRouter(page)}
                sx={{ my: 2, color: "#000000", display: "block",fontWeight:600, }}
              >
                {page}
              </Button>
            ))}
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
};

export default NavBar;
