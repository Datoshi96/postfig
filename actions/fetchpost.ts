"use server";
import { Post } from "@/models";
export async function fetchPost(page:number) {
    const perPage = 2;
    const apiUrl = `http://jsonplaceholder.typicode.com/posts?_start=${page}&_limit=${perPage}`;

    try {
        const response = await fetch(apiUrl);
        const data =  await response.json();
        return data as Post[];
    } catch (error) {
        console.error("Error fetching data:",error);
        return null;
    }
}