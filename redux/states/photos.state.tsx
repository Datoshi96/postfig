import { Photos } from "@/models";
import { createSlice } from "@reduxjs/toolkit";

export const PhotosEmptyState: Photos =
  {
    id: 0,
    title: '',
    url: '',
    thumbnailUrl: '',
  };

export const photosSlice = createSlice({
  name: "photos",
  initialState: PhotosEmptyState,
  reducers: {
    createPhotos: (state, action) => action.payload,
    modifyPhotos: (state, action) => ({ ...state, ...action.payload }),
    resetPhotos: () => PhotosEmptyState,
  },
});

export const {
  createPhotos,
  modifyPhotos,
  resetPhotos,
} = photosSlice.actions;

export default photosSlice.reducer;