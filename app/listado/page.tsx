"use client";
import {
  addNewPost,
  addPost,
  createPost,
  modifyPost,
  resetPost,
} from "@/redux/states/post.state";
import React, { useEffect, useState } from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import {
  Backdrop,
  Box,
  CardActions,
  CircularProgress,
  Grid,
  IconButton,
  TextField,
  Tooltip,
} from "@mui/material";
import { useAppDispatch, useAppSelector } from "@/redux/hooks";
import Divider from "@mui/material/Divider";
import VisibilityIcon from "@mui/icons-material/Visibility";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";

import { LoadMore } from "@/components";
import { fetchPost } from "@/actions/fetchpost";
import { Post } from "@/models";
import { modifyPage, resetPage } from "@/redux/states/page.state";
import { useRouter } from "next/navigation";
import { serviceDeletePost, serviceUpdatePost } from "@/services/services";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import Fab from "@mui/material/Fab";
import AddIcon from "@mui/icons-material/Add";

import styles from '@/styles/Home.module.css'

function post() {
  const router = useRouter();
  const postState = useAppSelector((state) => state.postReducer);
  const dispatch = useAppDispatch();
  const [states, setStates] = useState({
    open: false,
    openModalAdd: false,
    titleAdd: "",
    bodyAdd: "",
    idAdd: 100,
    openModalEdit: false,
    titleEdit: "",
    bodyEdit: "",
    idEdit: 0,
  });
  const [search, setSearch] = useState<string>("");
  const [filterData, setFilterData] = useState<Post[]>([]);

  const handleDeletePost = async (id: number) => {
    const newPostState = postState.filter((obj) => obj.id !== id);
    dispatch(createPost(newPostState));
    setFilterData(newPostState);
    await serviceDeletePost(id);
  };

  const removeAccents = (str: string) => {
    return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
  };

  const handleSearch = (e: any) => {
    setSearch(e.target.value);
    if (search.length > 1) {
      const filterValues = postState.filter((row) => {
        const rowTitle = removeAccents(row.title.toLowerCase());
        const t = removeAccents(e.target.value.toLowerCase());
        if (rowTitle.includes(t)) {
          return true;
        }
        return false;
      });
      console.log(filterValues);
      setFilterData(filterValues);
    } else {
      setFilterData(postState);
    }
  };

  const handleChangeAddPost = (e: any, type: string) => {
    if (type === "title") {
      setStates({
        ...states,
        titleAdd: e.target.value,
      });
    }
    if (type === "body") {
      setStates({
        ...states,
        bodyAdd: e.target.value,
      });
    }
  };

  const handleChangeEditPost = (e: any, type: string) => {
    if (type === "title") {
      setStates({
        ...states,
        titleEdit: e.target.value,
      });
    }
    if (type === "body") {
      setStates({
        ...states,
        bodyEdit: e.target.value,
      });
    }
  };

  const AddData = async () => {
    if(postState.length === 1){
      setStates({
        ...states,
        open: true,
      });
      dispatch(resetPage());
      dispatch(resetPost());
      try {
        const data = await fetchPost(1);
        if (Array.isArray(data)) {
          dispatch(createPost(data));
          setFilterData(data);
          setStates({
            ...states,
            open: false,
          });
        }
      } catch (error) {
        setStates({
          ...states,
          open: false,
        });
        console.log(error);
      }

    }
  };

  const handleGoPost = (p: Post) => {
    dispatch(modifyPage(p));
    router.push("/post");
  };

  const handleClose = () => {
    setStates({
      ...states,
      openModalAdd: false,
      openModalEdit: false,
    });
  };

  const handleAddPost = () => {
    const postAdd = {
      id: states.idAdd + 1,
      title: states.titleAdd,
      body: states.bodyAdd,
    };
    setStates({
      ...states,
      idAdd: states.idAdd + 1,
      openModalAdd: false,
    });
    dispatch(addNewPost(postAdd));
  };

  const handleClickAdd = () => {
    setStates({
      ...states,
      titleAdd: "",
      bodyAdd: "",
      openModalAdd: true,
    });
  };

  const handleEditPost = (p: Post) => {
    setStates({
      ...states,
      idEdit: p.id,
      titleEdit: p.title,
      bodyEdit: p.body,
      openModalEdit: true,
    });
  };

  const handleClickEditPost = async () => {
    const newPost = postState.map((post) => {
      if (post.id === states.idEdit) {
        return {
          ...post,
          title: states.titleEdit,
          body: states.bodyEdit,
        };
      }
      return post;
    });
    const bodyData = {
      id: states.idEdit,
      title: states.titleEdit,
      body: states.bodyEdit,
    };
    await serviceUpdatePost(states.idEdit, bodyData);
    dispatch(createPost(newPost));
    setStates({
      ...states,
      openModalEdit: false,
    });
  };

  useEffect(() => {
    if (search.length === 0) {
      setFilterData(postState);
    }
  }, [postState]);

  useEffect(() => {
    AddData();
  }, []);
  return (
    <>
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={states.open}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      <div>
        <h1 className={styles.h1}>Listado de Post</h1>
      </div>
        <Box
          component="form"
          // sx={{
          //   "& > :not(style)": { m: 1, width: "60ch" },
          // }}
          noValidate
          autoComplete="off"
          className={styles.grid}
        >
          <TextField
            color="warning"
            value={search}
            onChange={handleSearch}
            id="outlined-basic"
            label="Buscar"
            variant="outlined"
          />
        </Box>
      {filterData &&
        filterData.length > 0 &&
        filterData.map((post, index) => {
          return (
            <div key={index}>
              <Divider></Divider>
              <Card sx={{ maxWidth: 600, backgroundColor: "#FFD203" }}>
                <CardContent>
                  <Typography gutterBottom variant="h5" className={styles.text1} component="div">
                    {post.title}
                  </Typography>
                  <Typography variant="body2" className={styles.text2} color="text.secondary">
                    {post.body}
                  </Typography>
                </CardContent>
                <CardActions disableSpacing>
                  <Tooltip title="Ver post" placement="top-start">
                    <IconButton
                      onClick={() => handleGoPost(post)}
                      aria-label="go post"
                    >
                      <VisibilityIcon />
                    </IconButton>
                  </Tooltip>
                  <Tooltip title="Editar post" placement="top-start">
                    <IconButton
                      onClick={() => handleEditPost(post)}
                      aria-label="edit"
                    >
                      <EditIcon />
                    </IconButton>
                  </Tooltip>
                  <Tooltip title="Eliminar post" placement="top-start">
                    <IconButton
                      onClick={() => handleDeletePost(post.id)}
                      aria-label="delete"
                    >
                      <DeleteIcon />
                    </IconButton>
                  </Tooltip>
                </CardActions>
              </Card>
              <Divider></Divider>
            </div>
          );
        })}
      {filterData.length > 0 && search.length === 0 && <LoadMore></LoadMore>}
      <Dialog
        open={states.openModalAdd}
        onClose={handleClose}
        sx={{
          "& .MuiPaper-root": {
            background: "#FFEA98",
          },
        }}
      >
        <DialogTitle>Agregar Post</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Título"
            type="text"
            fullWidth
            color="warning"
            variant="outlined"
            value={states.titleAdd}
            onChange={(e) => handleChangeAddPost(e, "title")}
          />
          <TextField
            id="outlined-multiline-flexible"
            label="Contenido"
            multiline
            fullWidth
            color="warning"
            maxRows={5}
            value={states.bodyAdd}
            onChange={(e) => handleChangeAddPost(e, "body")}
          />
        </DialogContent>
        <DialogActions>
          <Button color="warning" onClick={handleClose}>
            Cancelar
          </Button>
          <Button color="warning" onClick={handleAddPost}>
            Agregar
          </Button>
        </DialogActions>
      </Dialog>
      <Dialog
        open={states.openModalEdit}
        onClose={handleClose}
        sx={{
          "& .MuiPaper-root": {
            background: "#FFEA98",
          },
        }}
      >
        <DialogTitle>Editar Post</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Título"
            type="text"
            fullWidth
            color="warning"
            variant="outlined"
            value={states.titleEdit}
            onChange={(e) => handleChangeEditPost(e, "title")}
          />
          <TextField
            id="outlined-multiline-flexible"
            label="Contenido"
            multiline
            fullWidth
            color="warning"
            maxRows={5}
            value={states.bodyEdit}
            onChange={(e) => handleChangeEditPost(e, "body")}
          />
        </DialogContent>
        <DialogActions>
          <Button color="warning" onClick={handleClose}>
            Cancelar
          </Button>
          <Button color="warning" onClick={handleClickEditPost}>
            Editar
          </Button>
        </DialogActions>
      </Dialog>
      <Tooltip title="Agregar Post" placement="top-start">
        <Box sx={{ position: "fixed", bottom: 16, right: 16 }}>
          <Fab color="warning" aria-label="add" onClick={handleClickAdd}>
            <AddIcon />
          </Fab>
        </Box>
      </Tooltip>
    </>
  );
}

export default post;
