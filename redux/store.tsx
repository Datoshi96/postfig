import { Post,Photos} from '@/models';
import { configureStore } from '@reduxjs/toolkit';
import { postSlice } from './states/post.state';
import { photosSlice } from './states/photos.state';
import postReducer from './states/post.state';
import { pageSlice } from './states/page.state';
import pageReducer from './states/page.state';
import photosReducer from './states/photos.state'
import filterReducer from './states/filter.state'

export const store = configureStore({
  reducer:{
    postReducer,
    pageReducer,
    photosReducer,
    filterReducer,
  }
});

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch

export const makeStore = () => {
  return configureStore({
    reducer: {
      post: postSlice.reducer,
      photos: photosSlice.reducer,
      page: pageSlice.reducer,
    }
  })
}
export type AppStore = ReturnType<typeof makeStore>