"use client";

import { useEffect, useState } from "react";
import { useInView } from "react-intersection-observer";
import { Box, CircularProgress } from "@mui/material";
import { fetchPost } from "@/actions/fetchpost";
import { useAppDispatch, useAppSelector } from "@/redux/hooks";
import { addPost } from "@/redux/states/post.state";

export default function LoadMore() {
  const [pagesLoaded, setPagesLoaded] = useState(0);
  const { ref, inView } = useInView();
  const dispatch = useAppDispatch();
  const postState = useAppSelector((state) => state.postReducer);
  const loadMorePost = async () => {
    if(postState.length>1){
      console.log("scrolled to the end");
      await new Promise(r=> setTimeout(r,2000));
      const nextPage = pagesLoaded+2;
      const newPosts = await fetchPost(nextPage) ?? [];
      newPosts.forEach((p)=>{
          dispatch(addPost(p));
      });
      setPagesLoaded(nextPage);
    }

  }

  useEffect(() => {
    if (inView) {
      loadMorePost();
    }
  }, [inView]);
  return (
    <>
      <div ref={ref}>
        <Box sx={{ display: "flex" }}>
          <CircularProgress color="inherit" />
        </Box>
      </div>
    </>
  );
}
