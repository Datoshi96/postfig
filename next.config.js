/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  experimental:{
    appDir:true,
    serverActions:true,
  },
  env:{
    API_URL: "https://jsonplaceholder.typicode.com",
  },
  // images:{
  //   remotePatterns:[],
  // }
}

module.exports = nextConfig
