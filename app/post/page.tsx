"use client";
import { useAppDispatch, useAppSelector } from "@/redux/hooks";
import { createPhotos, modifyPhotos } from "@/redux/states/photos.state";
import { serviceGetPhoto } from "@/services/services";
import { Card, CardContent, CardMedia, Typography } from "@mui/material";
import React, { useEffect } from "react";

export default function page() {
    const pageState = useAppSelector((state) => state.pageReducer);
    const photosState = useAppSelector((state) => state.photosReducer);
    const dispatch = useAppDispatch();
  const chargeData = async () => {
    const { data } = await serviceGetPhoto(pageState.id);
    dispatch(modifyPhotos(data));
  };
  useEffect(() => {
    chargeData();
  }, []);
  return (
    <div>
      <Card sx={{ maxWidth: 600 }}>
        <CardMedia
          component="img"
          height="221"
          image={photosState.url}
          alt={photosState.title}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            {pageState.title}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            {pageState.body}
          </Typography>
        </CardContent>
      </Card>
    </div>
  );
}
