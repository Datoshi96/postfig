import React from 'react'
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import styles from '@/styles/Home.module.css'

function App() {
  return (
    <>
    <div>
      <h1 className={styles.h1}>Bienvenido a PostFig</h1>
    </div>
    <Card sx={{ maxWidth: 600 }}>
        <CardMedia
          component="img"
          height="221"
          image="/image.png"
          alt="green iguana"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div" className={styles.text1}>
            PostFig
          </Typography>
          <Typography variant="body2" color="text.secondary" className={styles.text2}>
            En PostFig podrás crear, borrar, actualizar y listar tus publicaciones de una manera
            rápida e intuitiva.
          </Typography>
        </CardContent>
    </Card>

  </>
  )
}

export default App